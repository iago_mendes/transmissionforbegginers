---
title: "About"
date: 2022-10-09T17:22:53-03:00
draft: false
---

...

Este site irá mostrar tudo que você precisa saber para não ser um completo racheiro, conceitos básicos de transmissão e dinâmica veicular.

...

![Transmissão de um Formula](https://portfolium1.cloudimg.io/s/fit/1200x630/https://cdn.portfolium.com/ugcs3%2Fv3%2Fproject_attachments%2FdFLIBToESqCOMESSOfb6_DrivetrainRear.PNG)

...

Sobre mim

...

![Iago Mendes](https://uploaddeimagens.com.br/images/004/623/311/full/bf41efa5-eb30-44b5-93e9-b12f0a9a6e29.jpg?1696083851)

...

Em 2017 iniciei meus estudos no Anglo Cassiano Ricardo, onde cursei meu ensino médio. No ano de 2019 desenvolvi e apresentei meu trabalho de conclusão de ensino médio, sobre obsolescência programada.
No ano de 2021 iniciei a graduação na Escola Politécnica da Universidade de São Paulo, ingrssei no curso de engenharia de materiais, onde desenvolvi um trabalho de iniciação científica sobre variáveis da produção de ímãs de neodímio. Após 1 ano e meio realizei transferência interna para engenharia mecatrônica. Juntamente com a graduação, participo do grupo de extensão Equipe Poli Racing de Formula SAE no qual desenvolvi diferentes projetos, administração de projetos e membros de dinâmica veicular e atualmente desempenho o cargo de coordenador do subssistema de transmissão.

...

![Equipe](https://uploaddeimagens.com.br/images/004/623/318/original/equipe_epr._resized.jpg?1696084836)