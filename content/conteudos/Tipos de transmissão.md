---
title: "Tipos de transmissão"
date: 2022-10-09T17:41:35-03:00
draft: false
---
# Eixo cardan
#### O eixo cardan é normalmente utilizado em automóveis onde o motor se encontra na parta dianteira.

![Cardan](https://prakaranga.com.br/blog/wp-content/uploads/2023/02/O-que-e-e-como-funciona-o-Eixo-Cardan.png)

# Pinhão e Coroa

#### O sistema pinhão, corrente e coroa normalmente é utilizado em motocicletas e veículos com torque menos elevado. É tido como um sistema mais eficiente.

![Pinhãoecoroa](https://conectmotors.com.br/wp-content/uploads/2020/01/corrente-moto-1.jpg)

# Polias

#### As polias são utilizadas principalmente quando é necessário ter um fator de redução muito refinado.

![Polias](https://www.maestro.ind.br/wp-content/uploads/2019/09/Polias-e-engrenagens.jpg)

