---
title: "Performance de aceleração"
date: 2022-10-09T17:41:44-03:00
draft: false
---
## Quer extrair o melhor do seu carro? Entenda a hora certa de trocar de marcha!!

...

# Primeiro passo: Obtenha a curva de torque e potência do seu motor
#### A curva de torque mostra qual o torque oferecido pelo mootor para cada rotação

...

![curvadetorque)](https://blog.abmpecas.com/wp-content/uploads/2023/03/Saiba-mais-sobre-as-funcoes-de-torque-e-potencia.jpg)

...

# Segundo passo: Obtenha as especificações do seu carro
#### Tais como: inércia rotacional das partes girantes, massa e tamanho dos componentes, além da eficiência da transmissão.

...

# Terceiro passo: Monte o gráfico de força trativa x velocidade
#### Este gráfico serve para expor o comportamento do carro, os pontos onde as curvas se cruzam são os pontos ideais de troca de marcha.

...

![FtrxV](https://www.autoentusiastas.com.br/ae/wp-content/uploads/2014/09/for%C3%A7a-trativa.jpg)