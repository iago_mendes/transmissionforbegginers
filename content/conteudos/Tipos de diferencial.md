---
title: "Tipos de diferencial"
date: 2022-10-09T17:41:44-03:00
draft: false
---
### Quando se faz uma curva, o raio de curvatura das duas rodas trazeiras é diferente. Na prática, uma roda gira mais que a outra, então, se não quiser que seu carro só ande reto é melhor colocar um diferencial para distribuir o torque entre as duas rodas!!

...

# Diferencial Aberto

...

![diferencialaberto](https://www.razaoautomovel.com/wp-content/uploads/2011/11/diferencial-925x520.jpg)

...

#### Normalmente é utilizado em carros populares. É o que mais transfere o torque de uma roda para a outra, então, caso uma roda deslize, a maior parte do torque do motor será transferida para ela...

...

# Diferencial Torsen

...

![Torsen](https://d2t1xqejof9utc.cloudfront.net/screenshots/pics/f7ffd91f06b3720fdd586a5b278e3828/large.PNG)

...

#### O diferencial de sensoriamento de troque foi inventado para compensar o destracionamento de uma roda durante a transferencia de torque, e impedir que todo torque seja transferido para roda deslizante, mas é um tanto acima do peso......

...

# Diferencial de deslizamento limitado

...

![LSD](https://www.canaldapeca.com.br/blog/wp-content/uploads/2018/08/MINI-MN023-HA-2.jpg)

...

#### O diferencial de deslizamento limitado é a média entre os dois últimos! Ele é bem mais leve, e normalmente é possível ajustar a quantidade de torque que é transferida para cada roda!

...
